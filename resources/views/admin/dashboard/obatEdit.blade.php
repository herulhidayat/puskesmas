@extends('admin._layouts.master')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1>Edit Data Obat</h1>
            </div>
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="/dashboard">Home</a></li>
                <li class="breadcrumb-item active">Edit Data Obat</li>
              </ol>
            </div>
          </div>
        </div><!-- /.container-fluid -->
      </section>
  
      <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-12">
                <div class="card">
                  <div class="card-header">
                        <!-- Form -->
                    <form action="/obat/{{$obat->id_obat}}/update" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="formGroupExampleInput">Nama Obat</label>
                        <input name="nama_obat" type="text" class="form-control" id="formGroupExampleInput" placeholder="Nama Obat" value="{{$obat->nama_obat}}" required>
                        </div>
                        <div class="form-group">
                            <label for="formGroupExampleInput">Jenis Obat</label>
                            <input name="jenis_obat" type="text" class="form-control" id="formGroupExampleInput" placeholder="Jenis Obat" value="{{$obat->jenis_obat}}">
                        </div>
                        <div class="form-group">
                            <label for="formGroupExampleInput">Jumlah Obat</label>
                            <input name="jumlah_obat" type="int" class="form-control" id="formGroupExampleInput" placeholder="Jumlah Obat" value="{{$obat->jumlah_obat}}" required>
                        </div>
                        <div class="form-group">
                            <label for="formGroupExampleInput">Harga Obat</label>
                            <input name="harga_obat" type="text" class="form-control" id="formGroupExampleInput" placeholder="Harga Obat" value="{{$obat->harga_obat}}" required>
                        </div>
                        <button type="submit" class="btn btn-primary">Edit</button>
                    </form> 
                  </div>
                </div>
                <!-- /.card -->
              </div>
            </div>

    </section>
    <!-- /.content -->

@endsection