<!DOCTYPE html>
<html>
<!--Header-->
@include('admin._layouts.header')
<!--/.Header-->

<body class="hold-transition sidebar-mini">
  <!-- Site wrapper -->
  <div class="wrapper">
    <!-- Navbar -->
    @include('admin._layouts.navBar')
    <!-- /.navbar -->

    <!-- Main Sidebar Container -->
    @include('admin._layouts.sideBar')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
    @yield('content')
    </div>
    <!-- /.content-wrapper -->
    @include('admin._layouts.footer')
    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
      <!-- Control sidebar content goes here -->
    </aside>
    <!-- /.control-sidebar -->
  </div>
  <!-- ./wrapper -->

  <!-- Script -->
  @include('admin._layouts.script')
  <!-- ./Script -->
</body>
</html>
