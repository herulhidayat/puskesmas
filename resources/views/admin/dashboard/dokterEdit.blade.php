@extends('admin._layouts.master')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1>Edit Data Dokter</h1>
            </div>
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="/dashboard">Home</a></li>
                <li class="breadcrumb-item active">Edit Data Dokter</li>
              </ol>
            </div>
          </div>
        </div><!-- /.container-fluid -->
      </section>
  
      <!-- Main content -->
    <section class="content">
        <div class="row">
                <div class="col-12">
                    <div class="card">
                    <div class="card-header">
                        
                        <!-- Form -->
                        <form action="/dokter/{{$dokter->id_dokter}}/update" method="POST" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="formGroupExampleInput">Nama Dokter</label>
                            <input name="nama_dokter" type="text" class="form-control" id="formGroupExampleInput" placeholder="Nama Lengkap" value="{{$dokter->nama_dokter}}" required>
                            </div>
                            <div class="form-group">
                                <label for="formGroupExampleInput">NIP</label>
                                <input name="nip_dokter" type="text" class="form-control" id="formGroupExampleInput" placeholder="Nomor Induk Pegawai" value="{{$dokter->nip_dokter}}" required>
                            </div>
                            <div class="form-group">
                                <label for="formGroupExampleInput">Spesialis</label>
                                <input name="spesialis_dokter" type="text" class="form-control" id="formGroupExampleInput" placeholder="Spesialis" value="{{$dokter->spesialis_dokter}}" required>
                            </div>
                            <div class="form-group">
                                <label for="formGroupExampleInput">Tempat Lahir</label>
                                <input name="tempat_lahir_dokter" type="text" class="form-control" id="formGroupExampleInput" placeholder="Tempat Lahir" value="{{$dokter->tempat_lahir_dokter}}" required>
                            </div>
                            <div class="form-group">
                                <label>Tanggal Lahir</label>
                                <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                                </div>
                                <input name="tanggal_lahir_dokter" type="text" class="form-control" data-inputmask-alias="datetime" data-inputmask-inputformat="yyyy-mm-dd" data-mask value="{{$dokter->tanggal_lahir_dokter}}" required>
                                </div>
                            </div>
                            <div class="form-group">
                            <label for="exampleFormControlSelect1">Jenis Kelamin</label>
                            <select name="gender_dokter" class="form-control" id="exampleFormControlSelect1" required>
                                <option value="Laki-Laki" @if($dokter->gender_dokter == 'Laki-Laki') selected @endif>Laki-Laki</option>
                                <option value="Perempuan" @if($dokter->gender_dokter == 'Perempuan') selected @endif>Perempuan</option>
                            </select>
                            </div>
                            <div class="form-group">
                                <label for="formGroupExampleInput">Agama</label>
                                <input name="agama_dokter" type="text" class="form-control" id="formGroupExampleInput" placeholder="Agama" value="{{$dokter->agama_dokter}}">
                            </div>
                            <div class="form-group">
                                <label for="formGroupExampleInput">Nomor HP</label>
                                <input name="no_hp_dokter" type="text" class="form-control" id="formGroupExampleInput" placeholder="Nomor HP" value="{{$dokter->no_hp_dokter}}" required>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Email</label>
                                <input name="email_dokter" type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{$dokter->email_dokter}}" required>
                            </div>
                            <div class="form-group">
                                <label for="exampleFormControlTextarea1">Alamat</label>
                                <textarea name="alamat_dokter" class="form-control" id="exampleFormControlTextarea1" rows="3" required>{{$dokter->alamat_dokter}}</textarea>
                            </div>
                            <div class="form-group">
                                <label for="exampleFormControlFile1">Upload</label>
                                <input name="foto_dokter" type="file" class="form-control-file" id="exampleFormControlFile1" accept=".png, .jpg, .jpeg">
                            </div>
                            
                            <button type="submit" class="btn btn-primary">Edit</button>
                        </form>
                                
                    </div>

                    </div>
                    <!-- /.card -->
                </div>
            </div>

    </section>
    <!-- /.content -->

@endsection