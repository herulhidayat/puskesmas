<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/login', 'AuthController@login')->name('login');
Route::post('/postlogin', 'AuthController@postlogin');
Route::get('/logout', 'AuthController@logout');

Route::get('/user', 'UserController@index')->middleware('auth');
Route::post('/user/create', 'UserController@create')->middleware('auth');
Route::get('/user/{id}/edit', 'UserController@edit')->middleware('auth');
Route::post('/user/{id}/update', 'UserController@update')->middleware('auth');
Route::get('/user/{id}/delete', 'UserController@delete')->middleware('auth');
Route::get('/user/{id}/profile', 'UserController@profile')->middleware('auth');


Route::get('/admin', 'AdminController@dashboard')->middleware('auth');

Route::get('/dokter', 'DokterController@index')->middleware('auth');
Route::post('/dokter/create', 'DokterController@create')->middleware('auth');
Route::get('/dokter/{id_dokter}/edit', 'DokterController@edit')->middleware('auth');
Route::post('/dokter/{id_dokter}/update', 'DokterController@update')->middleware('auth');
Route::get('/dokter/{id_dokter}/delete', 'DokterController@delete')->middleware('auth');
Route::get('/dokter/{id_dokter}/profile', 'DokterController@profile')->middleware('auth');

Route::get('/perawat', 'PerawatController@index')->middleware('auth');
Route::post('/perawat/create', 'PerawatController@create')->middleware('auth');
Route::get('/perawat/{id_perawat}/edit', 'PerawatController@edit')->middleware('auth');
Route::post('/perawat/{id_perawat}/update', 'PerawatController@update')->middleware('auth');
Route::get('/perawat/{id_perawat}/delete', 'PerawatController@delete')->middleware('auth');
Route::get('/perawat/{id_perawat}/profile', 'PerawatController@profile')->middleware('auth');

Route::get('/obat', 'ObatController@index')->middleware('auth');
Route::post('/obat/create', 'ObatController@create')->middleware('auth');
Route::get('/obat/{id_obat}/edit', 'ObatController@edit')->middleware('auth');
Route::post('/obat/{id_obat}/update', 'ObatController@update')->middleware('auth');
Route::get('/obat/{id_obat}/delete', 'ObatController@delete')->middleware('auth');

Route::get('/pasien_umum', 'PasienController@index')->middleware('auth');
Route::post('/pasien/create', 'PasienController@create')->middleware('auth');
Route::get('/pasien_bpjs', 'AdminController@pasienBpjs')->middleware('auth');
