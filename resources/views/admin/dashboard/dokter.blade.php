@extends('admin._layouts.master')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1>Dokter</h1>
            </div>
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item active">Dokter</li>
              </ol>
            </div>
          </div>
          @if(session('sukses'))
      <div class="alert alert-success" role="alert">
        {{session('sukses')}}
      </div>
      @endif
        </div><!-- /.container-fluid -->
      </section>
  
      <!-- Main content -->
      <section class="content">
        
            <div class="row">
                    <div class="col-12">
                      <div class="card">
                        <div class="card-header">
                            <h3 class="card-title float-left">DataTable Dokter</h3>
                            <!-- Button trigger modal -->
                            <button type="button" class="btn btn-primary float-right btn-sm" data-toggle="modal" data-target="#exampleModal">
                                Tambah Data
                              </button>
                              
                              <!-- Modal -->
                              <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                      </button>
                                    </div>
                                    <div class="modal-body">
                                        <!-- Form -->
                                        <form action="/dokter/create" method="POST" enctype="multipart/form-data">
                                          {{ csrf_field() }}
                                          <div class="form-group">
                                              <label for="formGroupExampleInput">Nama Dokter</label>
                                              <input name="nama_dokter" type="text" class="form-control" id="formGroupExampleInput" placeholder="Nama Lengkap" required>
                                          </div>
                                          <div class="form-group">
                                              <label for="formGroupExampleInput">NIP</label>
                                              <input name="nip_dokter" type="text" class="form-control" id="formGroupExampleInput" placeholder="Nomor Induk Pegawai" required>
                                          </div>
                                          <div class="form-group">
                                              <label for="formGroupExampleInput">Spesialis</label>
                                              <input name="spesialis_dokter" type="text" class="form-control" id="formGroupExampleInput" placeholder="Spesialis" required>
                                          </div>
                                          <div class="form-group">
                                              <label for="formGroupExampleInput">Tempat Lahir</label>
                                              <input name="tempat_lahir_dokter" type="text" class="form-control" id="formGroupExampleInput" placeholder="Tempat Lahir" required>
                                          </div>
                                          <div class="form-group">
                                              <label>Tanggal Lahir</label>
                            
                                              <div class="input-group">
                                                <div class="input-group-prepend">
                                                  <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                                                </div>
                                                <input name="tanggal_lahir_dokter" type="text" class="form-control" data-inputmask-alias="datetime" data-inputmask-inputformat="yyyy-mm-dd" data-mask required>
                                              </div>
                                              <!-- /.input group -->
                                          </div>
                                          <div class="form-group">
                                            <label for="exampleFormControlSelect1">Jenis Kelamin</label>
                                            <select name="gender_dokter" class="form-control" id="exampleFormControlSelect1" required>
                                              <option>Laki-Laki</option>
                                              <option>Perempuan</option>
                                            </select>
                                          </div>
                                          <div class="form-group">
                                              <label for="formGroupExampleInput">Agama</label>
                                              <input name="agama_dokter" type="text" class="form-control" id="formGroupExampleInput" placeholder="Agama">
                                          </div>
                                          <div class="form-group">
                                              <label for="formGroupExampleInput">Nomor HP</label>
                                              <input name="no_hp_dokter" type="text" class="form-control" id="formGroupExampleInput" placeholder="Nomor HP" required>
                                          </div>
                                          <div class="form-group">
                                              <label for="exampleInputEmail1">Email</label>
                                              <input name="email_dokter" type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" required>
                                          </div>
                                          <div class="form-group">
                                              <label for="exampleFormControlTextarea1">Alamat</label>
                                              <textarea name="alamat_dokter" class="form-control" id="exampleFormControlTextarea1" rows="3" required></textarea>
                                          </div>
                                          <div class="form-group">
                                              <label for="exampleFormControlFile1">Upload</label>
                                              <input name="foto_dokter" type="file" class="form-control-file" id="exampleFormControlFile1">
                                          </div>
                                          
                                        
                                        </div>
                                        <div class="modal-footer">
                                          <button type="button" class="btn btn-secondary" data-dismiss="modal">Keluar</button>
                                          <button type="submit" class="btn btn-primary">Tambah</button>
                                        </div>
                                      </form>
                                  </div>
                                </div>
                              </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                          <table id="example2" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                              <th>Nama</th>
                              <th>NIP</th>
                              <th>Spesialis</th>
                              <th>Tempat Lahir</th>
                              <th>Tanggal Lahir</th>
                              <th>Jenis Kelamin</th>
                              <th>No. Hp</th>
                              <th>Email</th>
                              <th>Alamat</th>
                              <th>Edit/ Delete</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($data_dokter as $dokter)
                              <tr>
                                <td><a href="/dokter/{{$dokter->id_dokter}}/profile">{{$dokter->nama_dokter}}</a></td>
                                <td>{{$dokter->nip_dokter}}</td>
                                <td>{{$dokter->spesialis_dokter}}</td>
                                <td>{{$dokter->tempat_lahir_dokter}}</td>
                                <td>{{$dokter->tanggal_lahir_dokter}}</td>
                                <td>{{$dokter->gender_dokter}}</td>
                                <td>{{$dokter->no_hp_dokter}}</td>
                                <td>{{$dokter->email_dokter}}</td>
                                <td>{{$dokter->alamat_dokter}}</td>
                              <td><a href="/dokter/{{$dokter->id_dokter}}/edit" class="btn btn-warning btn-sm fa fa-edit float-left"></a><a href="/dokter/{{$dokter->id_dokter}}/delete" class="btn btn-danger btn-sm fa fa-trash float-right"></a></td>
                              </tr>  
                            @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                              <th>Nama</th>
                              <th>NIP</th>
                              <th>Spesialis</th>
                              <th>Tempat Lahir</th>
                              <th>Tanggal Lahir</th>
                              <th>Jenis Kelamin</th>
                              <th>No. Hp</th>
                              <th>Email</th>
                              <th>Alamat</th>
                              <th>Edit/ Delete</th>
                            </tr>
                            </tfoot>
                          </table>
                        </div>
                        <!-- /.card-body -->
                      </div>
                      <!-- /.card -->
                    </div>
                </div>
  
      </section>
      <!-- /.content -->

@endsection