@extends('admin._layouts.master')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1>Edit Data Perawat</h1>
            </div>
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="/dashboard">Home</a></li>
                <li class="breadcrumb-item active">Edit Data Perawat</li>
              </ol>
            </div>
          </div>
        </div><!-- /.container-fluid -->
      </section>
  
      <!-- Main content -->
    <section class="content">
        <div class="row">
                <div class="col-12">
                    <div class="card">
                    <div class="card-header">
                        
                        <!-- Form -->
                        <form action="/perawat/{{$perawat->id_perawat}}/update" method="POST" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="formGroupExampleInput">Nama Perawat</label>
                            <input name="nama_perawat" type="text" class="form-control" id="formGroupExampleInput" placeholder="Nama Lengkap" value="{{$perawat->nama_perawat}}" required>
                            </div>
                            <div class="form-group">
                                <label for="formGroupExampleInput">NIP</label>
                                <input name="nip_perawat" type="text" class="form-control" id="formGroupExampleInput" placeholder="Nomor Induk Pegawai" value="{{$perawat->nip_perawat}}" required>
                            </div>
                            <div class="form-group">
                                <label for="formGroupExampleInput">Tempat Lahir</label>
                                <input name="tempat_lahir_perawat" type="text" class="form-control" id="formGroupExampleInput" placeholder="Tempat Lahir" value="{{$perawat->tempat_lahir_perawat}}" required>
                            </div>
                            <div class="form-group">
                                <label>Tanggal Lahir</label>
                                <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                                </div>
                                <input name="tanggal_lahir_perawat" type="text" class="form-control" data-inputmask-alias="datetime" data-inputmask-inputformat="yyyy-mm-dd" data-mask value="{{$perawat->tanggal_lahir_perawat}}" required>
                                </div>
                            </div>
                            <div class="form-group">
                            <label for="exampleFormControlSelect1">Jenis Kelamin</label>
                            <select name="gender_perawat" class="form-control" id="exampleFormControlSelect1" required>
                                <option value="Laki-Laki" @if($perawat->gender_perawat == 'Laki-Laki') selected @endif>Laki-Laki</option>
                                <option value="Perempuan" @if($perawat->gender_perawat == 'Perempuan') selected @endif>Perempuan</option>
                            </select>
                            </div>
                            <div class="form-group">
                                <label for="formGroupExampleInput">Agama</label>
                                <input name="agama_perawat" type="text" class="form-control" id="formGroupExampleInput" placeholder="Agama" value="{{$perawat->agama_perawat}}">
                            </div>
                            <div class="form-group">
                                <label for="formGroupExampleInput">Nomor HP</label>
                                <input name="no_hp_perawat" type="text" class="form-control" id="formGroupExampleInput" placeholder="Nomor HP" value="{{$perawat->no_hp_perawat}}" required>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Email</label>
                                <input name="email_perawat" type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{$perawat->email_perawat}}" required>
                            </div>
                            <div class="form-group">
                                <label for="exampleFormControlTextarea1">Alamat</label>
                                <textarea name="alamat_perawat" class="form-control" id="exampleFormControlTextarea1" rows="3" required>{{$perawat->alamat_perawat}}</textarea>
                            </div>
                            <div class="form-group">
                                <label for="exampleFormControlFile1">Upload</label>
                                <input name="foto_perawat" type="file" class="form-control-file" id="exampleFormControlFile1" accept=".png, .jpg, .jpeg">
                            </div>
                            
                            <button type="submit" class="btn btn-primary">Edit</button>
                        </form>
                                
                    </div>

                    </div>
                    <!-- /.card -->
                </div>
            </div>

    </section>
    <!-- /.content -->

@endsection