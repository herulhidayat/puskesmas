@extends('admin._layouts.master')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1>Pasien Umum</h1>
            </div>
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item active">Pasien Umum</li>
              </ol>
            </div>
          </div>
        </div><!-- /.container-fluid -->
      </section>
  
      <!-- Main content -->
      <section class="content">
  
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                  <h3 class="card-title float-left">DataTable Pasien Umum</h3>
                  <!-- Button trigger modal -->
                  <button type="button" class="btn btn-primary float-right btn-sm" data-toggle="modal" data-target="#exampleModal">
                      Tambah Data
                    </button>
                    
                    <!-- Modal -->
                    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                      <div class="modal-dialog" role="document">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                          <div class="modal-body">
                              <!-- Form -->
                              <form action="/pasien/create" method="POST">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label for="formGroupExampleInput">Nama Pasien</label>
                                    <input name="nama_pasien" type="text" class="form-control" id="formGroupExampleInput" placeholder="Nama Pasien" required>
                                </div>
                                <div class="form-group">
                                  <label for="exampleFormControlSelect1">Jenis Layanan</label>
                                  <select name="jenis_pasien" class="form-control" id="exampleFormControlSelect1" required>
                                    <option value="0">Umum</option>
                                    <option value="1">BPJS</option>
                                  </select>
                                </div>
                                
                                <div class="form-group">
                                  <label for="formGroupExampleInput">Nomor Identitas</label>
                                  <input name="no_identitas_pasien" type="text" class="form-control" id="formGroupExampleInput" placeholder="KTP/SIM/Paspor" required>
                                </div>
                                <div class="form-group">
                                  <label for="formGroupExampleInput">Nomor BPJS</label>
                                  <input name="no_bpjs_pasien" type="text" class="form-control" id="formGroupExampleInput" placeholder="No. BPJS">
                                </div>
                                <div class="form-group">
                                  <label for="exampleFormControlSelect1">Jenis Kelamin</label>
                                  <select name="gender_pasien" class="form-control" id="exampleFormControlSelect1" required>
                                    <option>Laki-Laki</option>
                                    <option>Perempuan</option>
                                  </select>
                                </div>
                                <div class="form-group">
                                  <label for="formGroupExampleInput">Tempat Lahir</label>
                                  <input name="tempat_lahir_pasien" type="text" class="form-control" id="formGroupExampleInput" placeholder="Tempat Lahir" required>
                                </div>
                                <div class="form-group">
                                    <label>Tanggal Lahir</label>
                  
                                    <div class="input-group">
                                      <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                                      </div>
                                      <input name="tanggal_lahir_pasien" type="text" class="form-control" data-inputmask-alias="datetime" data-inputmask-inputformat="yyyy-mm-dd" data-mask required>
                                    </div>
                                    <!-- /.input group -->
                                </div>
                                <div class="form-group">
                                  <label for="formGroupExampleInput">Agama</label>
                                  <input name="agama_pasien" type="text" class="form-control" id="formGroupExampleInput" placeholder="Agama">
                                </div>
                                <div class="form-group">
                                    <label for="formGroupExampleInput">Nomor HP</label>
                                    <input name="no_hp_pasien" type="text" class="form-control" id="formGroupExampleInput" placeholder="Nomor HP" required>
                                </div>
                                <div class="form-group">
                                  <label for="formGroupExampleInput">Pekerjaan</label>
                                  <input name="pekerjaan_pasien" type="text" class="form-control" id="formGroupExampleInput" placeholder="Agama" required>
                                </div>
                                <div class="form-group">
                                    <label for="exampleFormControlTextarea1">Alamat</label>
                                    <textarea name="alamat_pasien" class="form-control" id="exampleFormControlTextarea1" rows="3" required></textarea>
                                </div>
                                <div class="form-group">
                                  <label for="exampleFormControlTextarea1">Keluhan</label>
                                  <textarea name="keluhan_pasien" class="form-control" id="exampleFormControlTextarea1" rows="3" ></textarea>
                              </div>

                              <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Keluar</button>
                                <button type="submit" class="btn btn-primary">Tambah</button>
                              </div>
                            </form>
                        </div>
                      </div>
                    </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example2" class="table table-bordered table-hover">
                  <thead>
                  <tr>
                    <th>Nama Pasien</th>
                    <th>No. Identitas</th>
                    <th>Jenis Kelamin</th>
                    <th>No. HP</th>
                    <th>Edit/ Delete</th>
                  </tr>
                  </thead>
                  <tbody>
                  @foreach ($data_pasien as $pasien)
                    <tr>
                      <td>{{$pasien->nama_pasien}}</td>
                      <td>{{$pasien->no_identitas_pasien}}</td>
                      <td>{{$pasien->gender_pasien}}</td>
                      <td>{{$pasien->no_hp_pasien}}</td>
                    <td><a href="/pasien/{{$pasien->id_pasien}}/edit" class="btn btn-warning btn-sm fa fa-edit float-left"></a><a href="/pasien/{{$pasien->id_pasien}}/delete" class="btn btn-danger btn-sm fa fa-trash float-right"></a></td>
                    </tr>  
                  @endforeach
                  </tbody>
                  <tfoot>
                  <tr>
                    <th>Nama Pasien</th>
                    <th>No. Identitas</th>
                    <th>Jenis Kelamin</th>
                    <th>No. HP</th>
                    <th>Edit/ Delete</th>
                  </tr>
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>



    </section>
    <!-- /.content -->
@endsection