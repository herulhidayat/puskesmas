@extends('admin._layouts.master')

@section('content')

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-3">

          <!-- Profile Image -->
          <div class="card card-primary card-outline">
            <div class="card-body box-profile">
              <div class="text-center">
                <img class="profile-user-img img-fluid img-circle"
                     src="{{$dokter->getFotodokter()}}"
                     alt="User profile picture">
              </div>

              <h3 class="profile-username text-center">{{$dokter->nama_dokter}}</h3>

            <p class="text-muted text-center">Spesialis {{$dokter->spesialis_dokter}}</p>

              <ul class="list-group list-group-unbordered mb-3">
                <li class="list-group-item">
                  <b>NIP</b> <a class="float-right">{{$dokter->nip_dokter}}</a>
                </li>
                <li class="list-group-item">
                  <b>Tempat Lahir</b> <a class="float-right">{{$dokter->tempat_lahir_dokter}}</a>
                </li>
                <li class="list-group-item">
                  <b>Tanggal Lahir</b> <a class="float-right">{{$dokter->tanggal_lahir_dokter}}</a>
                </li>
              </ul>

              <a href="/dokter/{{$dokter->id_dokter}}/edit" class="btn btn-primary btn-block"><b>Edit</b></a>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->

        </div>
        <!-- /.col -->
        <div class="col-md-9">
            <!-- About Me Box -->
          <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">Tentang</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <strong><i class="fas fa-envelope mr-1"></i> Email</strong>

              <p class="text-muted">
                {{$dokter->email_dokter}}
              </p>

              <hr>

              <strong><i class="fas fa-map-marker-alt mr-1"></i> Alamat</strong>
              <p class="text-muted">{{$dokter->alamat_dokter}}</p>
              <hr>
              <strong><i class="fas fa-phone mr-1"></i> Nomor Telepon</strong>
              <p class="text-muted">{{$dokter->no_hp_dokter}}</p>
              <hr>
              <strong><i class="fas fa-venus-mars mr-1"></i> Jenis Kelamin</strong>
              <p class="text-muted">{{$dokter->gender_dokter}}</p>
              <hr>
              <strong><i class="mr-1"></i>Agama</strong>
              <p class="text-muted">{{$dokter->agama_dokter}}</p>
            </div>
            <!-- /.card-body -->
          </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->

@endsection