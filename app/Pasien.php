<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pasien extends Model
{
    protected $table = 'pasien';
    protected $fillable = ['nama_pasien','jenis_pasien','no_identitas_pasien', 'no_bpjs_pasien','tempat_lahir_pasien','tanggal_lahir_pasien','gender_pasien','agama_pasien','no_hp_pasien','alamat_pasien','pekerjaan_pasien', 'keluhan_pasien'];
    protected $primaryKey = 'id_pasien';
}
