<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PerawatController extends Controller
{
    public function index() {
        $data_perawat = \App\Perawat::all();
        return view('admin.dashboard.perawat', ['data_perawat' => $data_perawat]);
    }
    public function create(Request $request) {
        $perawat = \App\Perawat::create($request->all());
        if($request->hasFile('foto_perawat')) {
            $request->file('foto_perawat')->move('adminPage/img/', $request->file('foto_perawat')->getClientOriginalName());
            $perawat->foto_perawat = $request->file('foto_perawat')->getClientOriginalName();
            $perawat->save();
        }
        return redirect('/perawat')->with('sukses','Data Berhasil Ditambahkan');
    }
    public function edit($id_perawat) {
        $perawat = \App\Perawat::find($id_perawat);
        return view('admin.dashboard.perawatEdit', ['perawat'=> $perawat]);
    }
    public function update(Request $request,$id_perawat) {
        $perawat = \App\Perawat::find($id_perawat);
        $perawat->update($request->all());
        if($request->hasFile('foto_perawat')) {
            $request->file('foto_perawat')->move('adminPage/img/', $request->file('foto_perawat')->getClientOriginalName());
            $perawat->foto_perawat = $request->file('foto_perawat')->getClientOriginalName();
            $perawat->save();
        }
        return redirect('/perawat')->with('sukses','Data Berhasil Diubah');

    }
    public function delete($id_perawat) {
        $perawat = \App\Perawat::find($id_perawat);
        $perawat->delete();
        return redirect('/perawat')->with('sukses','Data Berhasil Dihapus');
    }
    public function profile($id_perawat) {
        $perawat = \App\Perawat::find($id_perawat);
        return view('admin.dashboard.perawatProfile', ['perawat'=> $perawat]);
    }
}
