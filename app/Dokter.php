<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dokter extends Model
{
    protected $table = 'dokter';
    protected $fillable = ['nama_dokter','nip_dokter','spesialis_dokter','tempat_lahir_dokter','tanggal_lahir_dokter','gender_dokter','agama_dokter','no_hp_dokter','email_dokter','alamat_dokter','foto_dokter'];
    protected $primaryKey = 'id_dokter';

    public function getFotodokter() {
        if (!$this->foto_dokter) {
            return asset('adminPage/img/default.png');
        }
        return asset('adminPage/img/'.$this->foto_dokter);
    }
}
