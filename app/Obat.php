<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Obat extends Model
{
    protected $table = 'obat';
    protected $fillable = ['nama_obat','jenis_obat','jumlah_obat','harga_obat'];
    protected $primaryKey = 'id_obat';
}
