<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DokterController extends Controller
{
    public function index() {
        $data_dokter = \App\Dokter::all();
        return view('admin.dashboard.dokter', ['data_dokter' => $data_dokter]);
    }
    public function create(Request $request) {
        $dokter = \App\Dokter::create($request->all());
        if($request->hasFile('foto_dokter')) {
            $request->file('foto_dokter')->move('adminPage/img/', $request->file('foto_dokter')->getClientOriginalName());
            $dokter->foto_dokter = $request->file('foto_dokter')->getClientOriginalName();
            $dokter->save();
        }
        return redirect('/dokter')->with('sukses','Data Berhasil Ditambahkan');
    }
    public function edit($id_dokter) {
        $dokter = \App\Dokter::find($id_dokter);
        return view('admin.dashboard.dokterEdit', ['dokter'=> $dokter]);
    }
    public function update(Request $request,$id_dokter) {
        $dokter = \App\Dokter::find($id_dokter);
        $dokter->update($request->all());
        if($request->hasFile('foto_dokter')) {
            $request->file('foto_dokter')->move('adminPage/img/', $request->file('foto_dokter')->getClientOriginalName());
            $dokter->foto_dokter = $request->file('foto_dokter')->getClientOriginalName();
            $dokter->save();
        }
        return redirect('/dokter')->with('sukses','Data Berhasil Diubah');

    }
    public function delete($id_dokter) {
        $dokter = \App\Dokter::find($id_dokter);
        $dokter->delete();
        return redirect('/dokter')->with('sukses','Data Berhasil Dihapus');
    }
    public function profile($id_dokter) {
        $dokter = \App\Dokter::find($id_dokter);
        return view('admin.dashboard.dokterProfile', ['dokter'=> $dokter]);
    }
}
