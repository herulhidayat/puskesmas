@extends('admin._layouts.master')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1>Edit Data User</h1>
            </div>
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="/dashboard">Home</a></li>
                <li class="breadcrumb-item active">Edit Data User</li>
              </ol>
            </div>
          </div>
        </div><!-- /.container-fluid -->
      </section>
  
      <!-- Main content -->
    <section class="content">
        <div class="row">
                <div class="col-12">
                    <div class="card">
                    <div class="card-header">
                        
                        <!-- Form -->
                        <form action="/user/{{$user->id}}/update" method="POST" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="formGroupExampleInput">Nama Admin</label>
                                <input name="name" type="text" class="form-control" id="formGroupExampleInput" placeholder="Nama Lengkap" required>
                            </div>
                            <div class="form-group">
                              <label for="exampleInputEmail1">Email</label>
                              <input name="email" type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                              <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                            </div>
                            <div class="form-group">
                              <label for="exampleInputPassword1">Password</label>
                              <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                            </div>
                            <div class="form-group">
                                <label for="exampleFormControlFile1">Upload</label>
                                <input name="photo" type="file" class="form-control-file" id="exampleFormControlFile1">
                            </div>
                            
                            <button type="submit" class="btn btn-primary">Edit</button>
                        </form>
                                
                    </div>

                    </div>
                    <!-- /.card -->
                </div>
            </div>

    </section>
    <!-- /.content -->

@endsection