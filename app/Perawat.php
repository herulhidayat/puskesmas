<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Perawat extends Model
{
    protected $table = 'perawat';
    protected $fillable = ['nama_perawat','nip_perawat','tempat_lahir_perawat','tanggal_lahir_perawat','gender_perawat','agama_perawat','no_hp_perawat','email_perawat','alamat_perawat','foto_perawat'];
    protected $primaryKey = 'id_perawat';

    public function getFotoperawat() {
        if (!$this->foto_perawat) {
            return asset('adminPage/img/default.png');
        }
        return asset('adminPage/img/'.$this->foto_perawat);
    }
}
