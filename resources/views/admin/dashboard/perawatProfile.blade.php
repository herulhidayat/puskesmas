@extends('admin._layouts.master')

@section('content')

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-3">

          <!-- Profile Image -->
          <div class="card card-primary card-outline">
            <div class="card-body box-profile">
              <div class="text-center">
                <img class="profile-user-img img-fluid img-circle"
                     src="{{$perawat->getFotoperawat()}}"
                     alt="User profile picture">
              </div>

              <h3 class="profile-username text-center">{{$perawat->nama_perawat}}</h3>

            <p class="text-muted text-center">Spesialis {{$perawat->spesialis_perawat}}</p>

              <ul class="list-group list-group-unbordered mb-3">
                <li class="list-group-item">
                  <b>NIP</b> <a class="float-right">{{$perawat->nip_perawat}}</a>
                </li>
                <li class="list-group-item">
                  <b>Tempat Lahir</b> <a class="float-right">{{$perawat->tempat_lahir_perawat}}</a>
                </li>
                <li class="list-group-item">
                  <b>Tanggal Lahir</b> <a class="float-right">{{$perawat->tanggal_lahir_perawat}}</a>
                </li>
              </ul>

              <a href="/perawat/{{$perawat->id_perawat}}/edit" class="btn btn-primary btn-block"><b>Edit</b></a>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->

        </div>
        <!-- /.col -->
        <div class="col-md-9">
            <!-- About Me Box -->
          <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">Tentang</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <strong><i class="fas fa-envelope mr-1"></i> Email</strong>

              <p class="text-muted">
                {{$perawat->email_perawat}}
              </p>

              <hr>

              <strong><i class="fas fa-map-marker-alt mr-1"></i> Alamat</strong>
              <p class="text-muted">{{$perawat->alamat_perawat}}</p>
              <hr>
              <strong><i class="fas fa-phone mr-1"></i> Nomor Telepon</strong>
              <p class="text-muted">{{$perawat->no_hp_perawat}}</p>
              <hr>
              <strong><i class="fas fa-venus-mars mr-1"></i> Jenis Kelamin</strong>
              <p class="text-muted">{{$perawat->gender_perawat}}</p>
              <hr>
              <strong><i class="mr-1"></i>Agama</strong>
              <p class="text-muted">{{$perawat->agama_perawat}}</p>
            </div>
            <!-- /.card-body -->
          </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->

@endsection