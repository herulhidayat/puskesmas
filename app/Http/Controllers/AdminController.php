<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function dashboard() {
        return view('admin._layouts.content');
    }
    public function pasienUmum() {
        return view('admin.dashboard.pasienUmum');
    }
    public function pasienBpjs() {
        return view('admin.dashboard.pasienBpjs');
    }
}
