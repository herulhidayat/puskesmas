<!-- jQuery -->
<script src="/adminPage/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="/adminPage/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="/adminPage/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="/adminPage/js/demo.js"></script>
<!-- daterange picker -->
<link rel="stylesheet" href="/adminPage/plugins/daterangepicker/daterangepicker.css">
<!-- date-range-picker -->
<script src="/adminPage/plugins/daterangepicker/daterangepicker.js"></script>
<!-- InputMask -->
<script src="/adminPage/plugins/moment/moment.min.js"></script>
<script src="/adminPage/plugins/inputmask/min/jquery.inputmask.bundle.min.js"></script>
<!-- Font Awesome -->
<link rel="stylesheet" href="/adminPage/plugins/fontawesome-free/css/all.min.css">
<!-- Ionicons -->
<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">


<!--Data Tabel-->
    <!-- DataTables -->
    <script src="/adminPage/plugins/datatables/jquery.dataTables.js"></script>
    <script src="/adminPage/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>

<!-- page script -->
<script>
$(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
    "paging": true,
    "lengthChange": false,
    "searching": false,
    "ordering": true,
    "info": true,
    "autoWidth": false,
    });


    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('yyyy-mm-dd', { 'placeholder': 'yyyy-mm-dd' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({
      timePicker: true,
      timePickerIncrement: 30,
      locale: {
        format: 'MM/DD/YYYY hh:mm A'
      }
    })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )
});
</script>