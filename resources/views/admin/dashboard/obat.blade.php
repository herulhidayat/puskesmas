@extends('admin._layouts.master')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1>Obat</h1>
            </div>
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item active">Obat</li>
              </ol>
            </div>
          </div>
          @if(session('sukses'))
      <div class="alert alert-success" role="alert">
        {{session('sukses')}}
      </div>
      @endif
        </div><!-- /.container-fluid -->
      </section>
  
      <!-- Main content -->
      <section class="content">
  
          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-header">
                    <h3 class="card-title float-left">DataTable Obat</h3>
                    <!-- Button trigger modal -->
                    <button type="button" class="btn btn-primary float-right btn-sm" data-toggle="modal" data-target="#exampleModal">
                        Tambah Data
                      </button>
                      
                      <!-- Modal -->
                      <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                          <div class="modal-content">
                            <div class="modal-header">
                              <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                              </button>
                            </div>
                            <div class="modal-body">
                                <!-- Form -->
                                <form action="/obat/create" method="POST">
                                  {{ csrf_field() }}
                                  <div class="form-group">
                                      <label for="formGroupExampleInput">Nama Obat</label>
                                      <input name="nama_obat" type="text" class="form-control" id="formGroupExampleInput" placeholder="Nama Obat" required>
                                  </div>
                                  <div class="form-group">
                                    <label for="formGroupExampleInput">Jenis Obat</label>
                                    <input name="jenis_obat" type="text" class="form-control" id="formGroupExampleInput" placeholder="Jenis Obat">
                                  </div>
                                  <div class="form-group">
                                    <label for="formGroupExampleInput">Jumlah Obat</label>
                                    <input name="jumlah_obat" type="text" class="form-control" id="formGroupExampleInput" placeholder="Jumlah Obat" required>
                                  </div>
                                  <div class="form-group">
                                    <label for="formGroupExampleInput">Harga Obat</label>
                                    <input name="harga_obat" type="text" class="form-control" id="formGroupExampleInput" placeholder="Harga Obat" required>
                                  </div>
                                
                                </div>
                                <div class="modal-footer">
                                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Keluar</button>
                                  <button type="submit" class="btn btn-primary">Tambah</button>
                                </div>
                              </form>
                          </div>
                        </div>
                      </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                  <table id="example2" class="table table-bordered table-hover">
                    <thead>
                    <tr>
                      <th>Nama Obat</th>
                      <th>Jenis Obat</th>
                      <th>Jumlah</th>
                      <th>Harga</th>
                      <th>Edit/ Delete</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($data_obat as $obat)
                      <tr>
                        <td>{{$obat->nama_obat}}</td>
                        <td>{{$obat->jenis_obat}}</td>
                        <td>{{$obat->jumlah_obat}}</td>
                        <td>{{$obat->harga_obat}}</td>
                      <td><a href="/obat/{{$obat->id_obat}}/edit" class="btn btn-warning btn-sm fa fa-edit float-left"></a><a href="/obat/{{$obat->id_obat}}/delete" class="btn btn-danger btn-sm fa fa-trash float-right"></a></td>
                      </tr>  
                    @endforeach
                    </tbody>
                    <tfoot>
                    <tr>
                      <th>Nama Obat</th>
                      <th>Jenis Obat</th>
                      <th>Jumlah</th>
                      <th>Harga</th>
                      <th>Edit/ Delete</th>
                    </tr>
                    </tfoot>
                  </table>
                </div>
                <!-- /.card-body -->
              </div>
              <!-- /.card -->
            </div>
          </div>


  
      </section>
      <!-- /.content -->
@endsection