<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ObatController extends Controller
{
    public function index() {
        $data_obat = \App\Obat::all();
        return view('admin.dashboard.obat', ['data_obat' => $data_obat]);
    }
    public function create(Request $request) {
        $obat = \App\Obat::create($request->all());
        return redirect('/obat')->with('sukses','Data Berhasil Ditambahkan');
    }
    public function edit($id_obat) {
        $obat = \App\Obat::find($id_obat);
        return view('admin.dashboard.obatEdit', ['obat'=> $obat]);
    }
    public function update(Request $request,$id_obat) {
        $obat = \App\Obat::find($id_obat);
        $obat->update($request->all());
        return redirect('/obat')->with('sukses','Data Berhasil Diubah');
    }
    public function delete($id_obat) {
        $obat = \App\Obat::find($id_obat);
        $obat->delete();
        return redirect('/obat')->with('sukses','Data Berhasil Dihapus');
    }
}
