<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index() {
        $data_user = \App\User::all();
        return view('admin.dashboard.user', ['data_user' => $data_user]);
    }
    public function create(Request $request) {
        $user = \App\User::create($request->all());
        if($request->hasFile('photo')) {
            $request->file('photo')->move('adminPage/img/', $request->file('photo')->getClientOriginalName());
            $user->photo = $request->file('photo')->getClientOriginalName();
            $user->save();
        }
        return redirect('/user')->with('sukses','Data Berhasil Ditambahkan');
    }
    public function edit($id) {
        $user = \App\User::find($id);
        return view('admin.dashboard.userEdit', ['user'=> $user]);
    }
    public function update(Request $request,$id) {
        $user = \App\User::find($id);
        $user->update($request->all());
        if($request->hasFile('photo')) {
            $request->file('photo')->move('adminPage/img/', $request->file('photo')->getClientOriginalName());
            $user->photo = $request->file('photo')->getClientOriginalName();
            $user->save();
        }
        return redirect('/user')->with('sukses','Data Berhasil Diubah');

    }
    public function delete($id) {
        $user = \App\User::find($id);
        $user->delete();
        return redirect('/user')->with('sukses','Data Berhasil Dihapus');
    }
    public function profile($id) {
        $user = \App\User::find($id);
        return view('admin.dashboard.userProfile', ['user'=> $user]);
    }
}
