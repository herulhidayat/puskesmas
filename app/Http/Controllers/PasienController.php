<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PasienController extends Controller
{
    public function index() {
        $data_pasien = \App\Pasien::all();
        return view('admin.dashboard.pasienUmum', ['data_pasien' => $data_pasien]);
    }
    public function create(Request $request) {
        $pasien = \App\Pasien::create($request->all());
        return redirect('/pasienUmum')->with('sukses','Data Berhasil Ditambahkan');
    }
    public function edit($id_obat) {
        $obat = \App\Obat::find($id_obat);
        return view('admin.dashboard.pasienEdit', ['obat'=> $obat]);
    }
    public function update(Request $request,$id_obat) {
        $obat = \App\Obat::find($id_obat);
        $obat->update($request->all());
        return redirect('/obat')->with('sukses','Data Berhasil Diubah');
    }
    public function delete($id_pasien) {
        $pasien = \App\Pasien::find($id_pasien);
        $pasien->delete();
        return redirect('/obat')->with('sukses','Data Berhasil Dihapus');
    }
}
